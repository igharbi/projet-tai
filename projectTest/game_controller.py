import copy
import time

CHAMPION = 1
CHALLENGER = 2


class GameController:

    def __init__(self, game, champion, challenger):
        self.game = game
        self.champion = champion
        self.challenger = challenger
        self.trainingHistory = []

    def simulateManyGames(self, numberOfGames):
        champWins = 0
        challWins = 0
        draws = 0
        for i in range(numberOfGames):
            self.game.reset_game()
            self.playGame()
            if self.game.get_win() == CHAMPION:
                champWins = champWins + 1
            elif self.game.get_win() == CHALLENGER:
                challWins = challWins + 1
            else:
                draws = draws + 1
            print("games played: " + str(i+1) + "/" + str(numberOfGames))
        totalWins = champWins + challWins + draws
        print('Champion Wins: ' + str(int(champWins * 100 / totalWins)) + '%')
        print('Challenger Wins: ' + str(int(challWins * 100 / totalWins)) + '%')
        print('Draws: ' + str(int(draws * 100 / totalWins)) + '%')

    def playGame(self):
        playerToMove = self.champion if (self.champion.getPlayer() == self.game.get_turn()) else self.challenger
        while self.game.get_win() is None:
            availableMoves = self.game.getAvailableMoves()
            move = playerToMove.getMove(availableMoves, self.game.get_board())
            time.sleep(0.001)
            self.game.place(move[0])
            if playerToMove == self.champion:
                playerToMove = self.challenger
            else:
                playerToMove = self.champion

        for historyItem in self.game.get_boardHistory():
            self.trainingHistory.append((self.game.get_win(), copy.deepcopy(historyItem)))

    def getTrainingHistory(self):
        return self.trainingHistory
