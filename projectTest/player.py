import random
import copy

CHAMPION = 1
CHALLENGER = 2


class Player:

    def __init__(self, value, strategy='random', model=None, name="IA"):
        self.value = value
        self.strategy = strategy
        self.model = model
        self.name = name

    def getMove(self, availableMoves, board):
        if self.strategy == "random":
            return availableMoves[random.randrange(0, len(availableMoves))]
        else:
            maxValue = 0
            bestMove = availableMoves[0]
            for availableMove in availableMoves:
                boardCopy = copy.deepcopy(board)
                boardCopy[availableMove[0]][availableMove[1]] = self.value
                if self.value == CHAMPION:
                    value = self.model.predict(boardCopy, 2)
                else:
                    value = self.model.predict(boardCopy, 0)
                if value > maxValue:
                    maxValue = value
                    bestMove = availableMove
            return bestMove

    def getPlayer(self):
        return self.value

    def setPlayer(self, value):
        self.value = value

    def getModel(self):
        return self.model

    def getName(self):
        return self.name