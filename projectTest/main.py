from player import Player
from game_controller import GameController
from model import DNN
import time
from game import Connect4Game, Connect4Viewer, pygame
import threading

CHAMPION = 1
CHALLENGER = 2

if __name__ == "__main__":

    # Creating initial dataset by confronting two random player
    firstGame = Connect4Game()
    view = Connect4Viewer(game=firstGame)
    view.initialize()
    randChamp = Player(CHAMPION, strategy='random')
    randChall = Player(CHALLENGER, strategy='random')
    gameController = GameController(firstGame, randChamp, randChall)
    print("Playing with both players with random strategies")
    gameController.simulateManyGames(10)
    time.sleep(1)
    pygame.quit()

    """
        Building deep neural network and training with previously 
        generated dataset
    """
    model1 = DNN(42, 3, 50, 100)
    model1.train(gameController.getTrainingHistory())

    # First AI challenger
    LuigIA = Player(CHALLENGER, strategy='model', model=model1, name="LuigIA")

    # RandomPlayer vs LuigIA
    secondGame = Connect4Game()
    view = Connect4Viewer(game=secondGame)
    view.initialize()
    gameController = GameController(secondGame, randChamp, LuigIA)
    print("Playing with red player as Neural Network challenger LuigIA vs Random Player")
    gameController.simulateManyGames(10)
    time.sleep(1)
    pygame.quit()

    # Promoting Celia to champion
    LuigIA.setPlayer(CHAMPION)

    # Training second IA challenger
    model2 = model1
    model2.train(gameController.getTrainingHistory())
    MarIA = Player(CHALLENGER, strategy='model', model=model2, name="MarIA")

    ChampWinsCounter = 0
    champion = LuigIA
    challenger = MarIA

    while ChampWinsCounter < 3:
        game = Connect4Game()
        view = Connect4Viewer(game=game)
        view.initialize()
        gameController = GameController(game, champion, challenger)
        print(
            "Red player as challenger " + challenger.getName() + " vs Yellow player as champion " + champion.getName())
        gameController.simulateManyGames(10)
        time.sleep(1)
        pygame.quit()

        if game.get_win() == CHAMPION:
            ChampWinsCounter += 1
            print(champion.getName() + " is in win streak: " + str(ChampWinsCounter))
            # Training challenger for the rematch
            print("Training " + challenger.getName() + " for rematch!")
            challenger.getModel().train(gameController.getTrainingHistory())
        else:
            print("Former champion " + champion.getName() + " lost!")
            ChampWinsCounter = 0
            champion, challenger = challenger, champion
            champion.setPlayer(CHAMPION)
            challenger.setPlayer(CHALLENGER)
            print("New champion " + champion.getName() + "!")
            print("Training " + challenger.getName() + " for rematch!")
            challenger.getModel().train(gameController.getTrainingHistory())
